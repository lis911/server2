<?php
require_once 'connection.php';

$date = date('Y.m.d H:i:s');
$response = '';
$postInput = null;
$postCheckPhp = null;
$postCheckTxt = null;

if (!empty($_POST)) {
    $postInput = $_POST['find'] ?? null;
    $extensions = $_POST['extensions'] ?? null;

    if (!$postInput || !$extensions) {
        die('Find string or extension cannot be blank.');
    }

    $link = mysqli_connect($host, $user, $password, $database)
    or die('Ошибка ' . mysqli_error($link));

    $extensionsString = implode(' ', $extensions);
    $query = "INSERT INTO history_request(request, date, extension) VALUES ('{$postInput}', '{$date}', '{$extensionsString}')";
    $result = mysqli_query($link, $query) or die('Ошибка ' . mysqli_error($link));
    if ($result == false) {
        echo 'Запрос не выполнился';
    }
    mysqli_close($link);

    $myCurl = curl_init();
    curl_setopt_array($myCurl, [
        CURLOPT_URL => 'http://server1.webdev',
        CURLOPT_RETURNTRANSFER => true,
        CURLOPT_POST => true,
        CURLOPT_POSTFIELDS => http_build_query($_POST)
    ]);
    $response = curl_exec($myCurl);
    curl_close($myCurl);
}
?>

<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta1/dist/css/bootstrap.min.css" rel="stylesheet"
          integrity="sha384-giJF6kkoqNQ00vy+HMDP7azOuL0xtbfIcaT9wjKHr8RbDVddVHyTfAAsrekwKmP1" crossorigin="anonymous">
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta1/dist/js/bootstrap.bundle.min.js"
            integrity="sha384-ygbV9kiqUc6oa4msXn9868pTtWMgiQaeYH7/t7LECLbyPA2x65Kgf80OJFdroafW"
            crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.5.4/dist/umd/popper.min.js"
            integrity="sha384-q2kxQ16AaE6UbzuKqyBE9/u/KzioAlnx2maXQHiDX9d4/zp8Ok3f+M7DPm+Ib6IU"
            crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta1/dist/js/bootstrap.min.js"
            integrity="sha384-pQQkAEnwaBkjpqZ8RU1fF1AKtTcHJwFl3pblpTlHXybJjHpMYo79HY3hIi4NKxyj"
            crossorigin="anonymous"></script>

    <title>Server2</title>
</head>
<body>


<div class="container overflow-hidden">
    <div class="row gx-5">
        <div class="col">
            <div class="p-3 border bg-light">
                <form method="post" action="index.php">
                    <input type="text" name="find" placeholder ="Введите ключевое слово:">
                    <input type="submit" alt="Поиск"><br>
                    <input type="checkbox" name="extensions[]" value="php"> .php
                    <input type="checkbox" name="extensions[]" value="txt"> .txt
                    <input type="checkbox" name="extensions[]" value="log"> .log
                </form></div>
        </div>
        <div class="col">
            <?php  ?>
        </div>
    </div>
</div>

<div class="container overflow-hidden">
    <div class="row gx-5">
        <div class="col">
            <div class="p-3 border bg-light">
                <b>Ответ на запрос:</b><br>
                <div class="p-3 border bg-light"> <?= "$response <br>" ?></div>
               </div>
        </div>
        <div class="col">
        </div>
    </div>
</div>
</body>
</html>

